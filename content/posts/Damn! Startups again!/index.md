---
title: Damn! Startups again!
date: 2023-11-01
---


A recent observation while talking to my founder pals: firing is part of the early startup journey. It’s tough because not everyone fits in, and as a founder, your role isn’t to mold them but to find those who naturally align. You’re in the game to find those who vibe with your vision, not to run a training camp. The deal is simple: treat the keepers well, be fair to the others with a decent severance, but then you’ve gotta let go. Early-stage founding isn’t about coaching; it’s about finding the right fit fast.

The beauty of scaling a startup? Your magnetism for attracting top talent grows with you. In those early, gritty days, it’s your passion that counts more than the heavy hitters you can recruit, especially in leadership. Start with the core, the hands-on team. When the time’s right, and your startup’s buzzing, that’s when you bring in the seasoned managers.
